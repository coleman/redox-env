# redox-env

Ion script to print useful info for bug reports.

## Usage

You must have Ion. And you must run this from the **root of the main Redox repo**. 
Download it and symlink it on your PATH, or just curl-to-ion like a boss.

```
curl --silent https://gitlab.redox-os.org/coleman/redox-env/raw/master/redox-env | ion
```

